# Gitbook搭建

* **下载nodejs**

1.官网:[https://nodejs.org/en/](https://nodejs.org/en/)

2.下载msi安装包

3.安装

4.输入node --version，验证是否安装成功

![](.gitbook/assets/1.png)

* **搭建gitbook平台**

### 1.打开管理员cmd

### 2.输入命令,全局安装gitbook

cnpm install gitbook-cli -g

或者

npm install gitbook-cli -g

## 3.建立文件夹

切换到任意目录下，创建文件xxxx

mkdir xxxx

## 4.初始化目录文件

gitbook init

## 5.编译书籍 gitbook build

## 6. gitbook serve

## 7.下载gitbook editor

\_\_[_https://legacy.gitbook.com/editor_](https://legacy.gitbook.com/editor)\_\_



### 更换镜像（三种方式） {#更换镜像三种方式}

1.通过config命令

> npm config set registry [https://registry.npm.taobao.org](https://registry.npm.taobao.org/) –global   
> npm config set disturl [https://npm.taobao.org/dist](https://npm.taobao.org/dist) –global

2.命令行指定

> npm –registry [https://registry.npm.taobao.org](https://registry.npm.taobao.org/) info underscore

3.编辑 ~/.npmrc 加入下面内容

> registry = [https://registry.npm.taobao.org](https://registry.npm.taobao.org/)

