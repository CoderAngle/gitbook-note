# Gitbook PDF

输出pdf文件步骤如下:

* 由于生成PDF文件依赖于`ebook-convert,`故首先在该处[ebook-convert下载链接](http://calibre-ebook.com/download)点击下载所需要的版本，安装即可;
* 打开cmd，进入E:\gitbook目录;
* E:\gitbook&gt;gitbook pdf gitbook-studying gitbook-studying/gitbook入门教程.pdf
* 则在目录E:\gitbook\gitbook-studying下生成了该pdf文件

