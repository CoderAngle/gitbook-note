# Gitbook Advanced

* **插件**
* **disqus**

npm install gitbook-plugin-disqus -g

然后，修改`book.json`配置文件，添加插件的配置内容：

```text
{
    "plugins": ["disqus"],
    "pluginsConfig": {
        "disqus": {
            "shortName": "introducetogitbook"
        }
    }  
}
```

* **multipart**

npm install gitbook-plugin-multipart -g

然后编辑`book.json`添加 multipart 到 plugins 中：

```text
{
  "plugins": [
        "multipart"
    ],
}
```

* **toggle-chapters**

npm insatll gitbook-plugin-toggle-chapters

然后编辑`book.json`添加 toggle-chapters 到 plugins 中：

```text
{
    "plugins": [
    "toggle-chapters"
    ],
}
```

* **gitbook-plugin-codeblock-filename**

npm insatll gitbook-plugin-codeblock-filename --save

然后编辑`book.json`添加 codeblock-filename 到 plugins 中：

```text
{
    plugins: [ 
    "codeblock-filename"
 ] ,
}
```

