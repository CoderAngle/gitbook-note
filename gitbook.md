# Gitbook 简介&使用

* **简介**

## **README.md 和 SUMMARY.md 是两个必须文件**

### README.md 是对书籍的简单介绍

### SUMMARY.md 是书籍的目录结构

* **克隆书籍源代码**

git clone path

* **编辑内容**

git commit -asm "init book"

* **发布内容**

git push

